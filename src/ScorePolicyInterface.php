<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-score-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Score;

use Stringable;

/**
 * ScorePolicyInterface interface file.
 *
 * A Score Policy is an algorithm which is capable of reducing a collection
 * of scores down to an unique score by absorbing their values.
 * 
 * @author Anastaszor
 */
interface ScorePolicyInterface extends Stringable
{
	
	/**
	 * Absorbs the given score values.
	 *
	 * @param ScoreInterface $score
	 * @param integer $weight
	 * @return boolean whether the value was correctly absorbed
	 */
	public function absorb(ScoreInterface $score, int $weight = 1) : bool;
	
	/**
	 * Gets the current value merged score as it is now.
	 *
	 * @return ScoreInterface
	 */
	public function getCurrentValue() : ScoreInterface;
	
}
