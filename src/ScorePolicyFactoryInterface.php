<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-score-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Score;

use Stringable;

/**
 * ScorePolicyFactoryInterface interface file.
 * 
 * A score factory is a delegate that will create scores policies to be
 * manipulated as a group.
 * 
 * @author Anastaszor
 */
interface ScorePolicyFactoryInterface extends Stringable
{
	
	/**
	 * Builds a new score policy with the given preconfigured parameters.
	 * 
	 * @return ScorePolicyInterface
	 */
	public function createScorePolicy() : ScorePolicyInterface;
	
}
