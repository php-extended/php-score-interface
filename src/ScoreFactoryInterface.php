<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-score-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Score;

use InvalidArgumentException;
use Stringable;

/**
 * ScoreFactoryInterface interface file.
 * 
 * A score factory is a delegate that will create scores from values for
 * the scores to be manipulated as a group.
 * 
 * @author Anastaszor
 */
interface ScoreFactoryInterface extends Stringable
{
	
	/**
	 * Creates a new score that is specific to this factory.
	 * 
	 * @param array<integer|string, null|boolean|integer|float|string> $values
	 * @return ScoreInterface
	 * @throws InvalidArgumentException if the values do not match the
	 *                                  values that are expected from the score to create
	 */
	public function createScore(array $values = []) : ScoreInterface;
	
}
