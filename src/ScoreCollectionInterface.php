<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-score-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Score;

use Stringable;

/**
 * ScoreCollectionInterface interface file.
 *
 * A Score Collection is a collection of scores which can be reduced down to
 * an unique score usign a certain way of calculating the fusion of all those
 * scores.
 * 
 * @author Anastaszor
 */
interface ScoreCollectionInterface extends Stringable
{
	
	/**
	 * Adds the score to the collection.
	 *
	 * @param ScoreInterface $score
	 * @param integer $weight
	 * @return boolean whether the collection has absorbed the score
	 */
	public function add(ScoreInterface $score, int $weight = 1) : bool;
	
	/**
	 * Merges all the scores of the collection into the policy, and gets back
	 * the result.
	 *
	 * @param ScorePolicyInterface $policy
	 * @return ScoreInterface the merged result
	 */
	public function resolve(ScorePolicyInterface $policy) : ScoreInterface;
	
}
