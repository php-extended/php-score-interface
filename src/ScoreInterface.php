<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-score-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Score;

use Stringable;

/**
 * ScoreInterface interface file.
 *
 * A score is a mesurement of a performance according to certain rules.
 * Such score is a score that is defined in terms of points on the line
 * of reals. A typical score is a float defined to be between 0 and 1,
 * and the nearer of 1 the score is, the more accurate is the precision of the
 * measured attribute. At the opposite, the nearer of 0 the score is, the less
 * accurate is the precision of the measured attribute.
 * 
 * @author Anastaszor
 */
interface ScoreInterface extends Stringable
{
	
	/**
	 * Gets the minimal value that is allowed for this score as object.
	 * The current score value should be greater or equals to this minimal
	 * value.
	 *
	 * @return float
	 */
	public function getMinValue() : float;
	
	/**
	 * Gets the maximal value that is allowed for this score as object.
	 * The current score value should be lowed or equals to this maximal
	 * value.
	 *
	 * @return float
	 */
	public function getMaxValue() : float;
	
	/**
	 * Gets the current value of this score. This score should be between,
	 * inclusive, the maximan and the minimal value.
	 *
	 * @return float
	 */
	public function getCurrentValue() : float;
	
	/**
	 * Gets the normalized value of this score. This score MUST be between
	 * 0 and 1, inclusive.
	 *
	 * @return float
	 */
	public function getNormalizedValue() : float;
	
	/**
	 * Compares another score to this one.
	 *
	 * @param ScoreInterface $score
	 * @return integer negative if this score is lower than given score,
	 *                 zero if it is equal,
	 *                 positive if this score is greater than given score
	 */
	public function comparesTo(ScoreInterface $score) : int;
	
}
